/**
 * Invoices.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    parser_id : { type: "string" },
    document_id : { type: "string" },
    remote_id : { type: "string" },
    file_name : { type: "string" },
    media_link : { type: "string" },
    media_link_original : { type: "string" },
    media_link_data : { type: "string" },
    uploaded_at : { type: 'string', columnType: 'date' },
    processed_at : { type: 'string', columnType: 'date' },
    invoice_date : { type: "json" },
    ship_date : { type: "json" },
    invoice_number : { type: "string" },
    upc : { type: "json" },
    vendor_item_code : { type: "json" },
    sales_person : { type: "json" },
    terms : { type: "json" },
    line_items : { type: "json" },
    page : { type: "string" }

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};

