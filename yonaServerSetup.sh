echo "initial yum updates and installs"
#yum check-update
yum -y update

echo "now installing all default tools"
yum install -y git nano policycoreutils policycoreutils-python selinux-policy selinux-policy-targeted libselinux-utils setroubleshoot-server setools setools-console mcstrans iptables-service

service iptabls stop
systemctl disable iptables

# disable firewall first if not already
chkconfig iptables off

yum install -y git wget ntp gcc net-tools iptables-services yum-cron sshfs epel-release openssl libstdc++ libssh2 libssh2-devel libssh2-docs libssh2 libssh2-devel \

echo "#######################################"
echo "install and enable yum-cron"
echo "#######################################"
yum update -y
systemctl enable yum-cron
nano /etc/yum/yum-cron.conf

#update_cmd = security
#apply_updates = yes

systemctl start yum-cron
systemctl enable ntpd
systemctl start ntpd
timedatectl set-timezone 'America/New_York'

echo "#######################################"
echo "installing ec2 region specific tools"
echo "#######################################"

yum -y install yum-utils
yum-config-manager --enable rhui-us-east-1b-rhel-server-extras rhui-us-east-1b-rhel-server-optional

#echo "install and enable fail2ban"
#yum install -y fail2ban
#systemctl enable fail2ban

#echo "[DEFAULT]" > /etc/fail2ban/jail.local
#echo "# Set a 1 hour ban" >> /etc/fail2ban/jail.local
#echo "bantime = 3600" >> /etc/fail2ban/jail.local
#echo "" >> /etc/fail2ban/jail.local
#echo "# Override /etc/fail2ban/jail.d/00-firewalld.conf:" >> /etc/fail2ban/jail.local
#echo "banaction = iptables-multiport" >> /etc/fail2ban/jail.local
#echo "" >> /etc/fail2ban/jail.local
#echo "[sshd]" >> /etc/fail2ban/jail.local
#echo "enabled = true" >> /etc/fail2ban/jail.local

#check that all is well!
#nano /etc/fail2ban/jail.local

# nodejs
curl -sL https://rpm.nodesource.com/setup_10.x | sudo -E bash -
yum -y install nodejs

echo "[mongodb-org-3.6]" > /etc/yum.repos.d/mongodb-org.repo
echo "name=MongoDB Repository" >> /etc/yum.repos.d/mongodb-org.repo
echo "baseurl=https://repo.mongodb.org/yum/redhat/7/mongodb-org/3.6/x86_64/" >> /etc/yum.repos.d/mongodb-org.repo
echo "gpgcheck=1" >> /etc/yum.repos.d/mongodb-org.repo
echo "enabled=1" >> /etc/yum.repos.d/mongodb-org.repo
echo "gpgkey=https://www.mongodb.org/static/pgp/server-3.6.asc" >> /etc/yum.repos.d/mongodb-org.repo

yum -y install mongodb-org
systemctl enable mongod
systemctl start mongod

echo "#######################################"
echo "verifying node and npm versions"
echo "#######################################"
node -v
npm -v

echo "#######################################"
echo "setting up project folders"
echo "#######################################"

mkdir /data
mkdir /data/.webhome
cd /data/.webhome
mkdir rinconcap
cd rinconcap
mkdir private logs web

#echo "cloning project from git"
#git clone https://nateschilling:42dd3ddb7956df33c13d74081bf52e82f5310ba0@github.com/KMustakas88/onyx_v2.git web

#echo "setting up onyx project"
#cd web/onyx_app/
#npm install
npm install sails -g
npm install forever -g

echo "#######################################"
echo "setting up nginx"
echo "#######################################"

yum -y install nginx certbot-nginx

mkdir /etc/nginx/sites-available
mkdir /etc/nginx/sites-enabled
touch rinconcap.conf
mv rinconcap.conf /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/rinconcap.conf /etc/nginx/sites-enabled/rinconcap.conf

echo "include /etc/nginx/sites-enabled/*.conf;" >> /etc/nginx/nginx.conf
#echo "server_names_hash_bucket_size 64;" >> /etc/nginx/nginx.conf

# nano /etc/nginx/nginx.conf
# add at end of html block
# include /etc/nginx/sites-enabled/*.conf;
# server_names_hash_bucket_size 64;

service ngninx restart

certbot --authenticator standalone --installer nginx --pre-hook "nginx -s stop" --post-hook "nginx"
